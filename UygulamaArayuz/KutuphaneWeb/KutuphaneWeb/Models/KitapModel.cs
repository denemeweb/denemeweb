﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KutuphaneWeb.Models
{
    public class KitapModel
    {
        public string yayinEviAdi { get; set; }
        public string yazarAdi { get; set; }
        public string yazarSoyadi { get; set; }
        public string turId { get; set; }
        public string yazarAdiSoyadi { get; set; }
        public DateTime yayinTarihiBas { get; set; }
        public DateTime yayinTarihiSon { get; set; }
        public int kitapId { get; set; }
        public string isbnKod { get; set; }
        
        public string kitapAdi { get; set; }
        public string yazarId { get; set; }
        public string yayinEviId { get; set; }
        public string kutuphaneId { get; set; }
        public string yayinTarihi { get; set; }
        public string sayfaSayisi { get; set; }
        public string kayitTarihi { get; set; }
        public string kullaniciId { get; set; }
        public string guncellemeTarihi { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KutuphaneWeb.Models
{
    public class KitapOduncModel
    {
        public  int kitapID { get; set; }
        public string isbnKod;
        public string yazarAdi { get; set; }
        public string yazarSoyadi { get; set; }
        public string turId { get; set; }
        public string kullaniciAdi { get; set; }
        public int kitapOduncId { get; set; }

        public string kitapAdi { get; set; }
        public string kitapId { get; set; }
        public string uyeId { get; set; }
        public string kullaniciId { get; set; }
        public string teslimTarihi { get; set; }
        public string emanetTarihi { get; set; }
        public string songunTarihi { get; set; }
        public bool oduncDurumu { get; set; }
        public string uyeAdiSoyadi { get; set; }
        public string durum { get; set; }
        public string kayitTarihi { get; set; }
        public string guncellemeTarihi { get; set; }

        public string yazarAdiSoyadi { get; set; }

        public string sayfaSayisi { get; set; }

        public string yayinEviAdi { get; set; }

        public string yayinTarihi { get; set; }

        public string statuString { get; set; }

        public int SecilenDurum { get; set; }
        public IEnumerable<DurumListesi> DurumList
        {
            get
            {
                return new[]
            {
                new DurumListesi { Value = "0", Text = "Durum Seçiniz" },
                new DurumListesi { Value = "1", Text = "Ödünç Verildi" },
                new DurumListesi { Value = "2", Text = "Teslim Alındı" },
            };
            }
        }
    }
}
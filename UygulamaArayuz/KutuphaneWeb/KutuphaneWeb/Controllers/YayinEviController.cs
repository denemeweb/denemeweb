﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KutuphaneWeb.Models;

namespace KutuphaneWeb.Controllers
{
    public class YayinEviController : Controller
    {
        private KutuphaneDBEntities db = new KutuphaneDBEntities();

        // GET: /YayinEvi/
        public ActionResult Index()
        {
            return View(db.YAYIN_EVI.ToList());
        }

        // GET: /YayinEvi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YAYIN_EVI yayin_evi = db.YAYIN_EVI.Find(id);
            if (yayin_evi == null)
            {
                return HttpNotFound();
            }
            return View(yayin_evi);
        }

        // GET: /YayinEvi/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /YayinEvi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="yayinEviId,yayinEviAdi,adress,kayitTarihi,kullaniciId,guncellemeTarihi")] YAYIN_EVI yayin_evi)
        {
            yayin_evi.kayitTarihi = DateTime.Now;
            yayin_evi.kullaniciId = User.Identity.GetUserName();
            if (ModelState.IsValid)
            {
                db.YAYIN_EVI.Add(yayin_evi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(yayin_evi);
        }

        // GET: /YayinEvi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YAYIN_EVI yayin_evi = db.YAYIN_EVI.Find(id);
            if (yayin_evi == null)
            {
                return HttpNotFound();
            }
            return View(yayin_evi);
        }

        // POST: /YayinEvi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="yayinEviId,yayinEviAdi,adress,kayitTarihi,kullaniciId,guncellemeTarihi")] YAYIN_EVI yayin_evi)
        {
            yayin_evi.guncellemeTarihi = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(yayin_evi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(yayin_evi);
        }

        // GET: /YayinEvi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YAYIN_EVI yayin_evi = db.YAYIN_EVI.Find(id);
            if (yayin_evi == null)
            {
                return HttpNotFound();
            }
            return View(yayin_evi);
        }

        // POST: /YayinEvi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            YAYIN_EVI yayin_evi = db.YAYIN_EVI.Find(id);
            db.YAYIN_EVI.Remove(yayin_evi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

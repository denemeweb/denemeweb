﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Web.Mvc;
using KutuphaneWeb.Models;

namespace KutuphaneWeb.Controllers
{
    public class YazarController : Controller
    {
        private KutuphaneDBEntities db = new KutuphaneDBEntities();

        // GET: /Yazar/
        public ActionResult Index()
        {
            return View(db.YAZAR.ToList());
        }

        // GET: /Yazar/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YAZAR yazar = db.YAZAR.Find(id);
            if (yazar == null)
            {
                return HttpNotFound();
            }
            return View(yazar);
        }

        // GET: /Yazar/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Yazar/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="yazarId,yazarAdi,yazarSoyadi,kayitTarihi,kullaniciId,guncellemeTarihi")] YAZAR yazar)
        {
            yazar.kayitTarihi = DateTime.Now;
            yazar.kullaniciId = User.Identity.GetUserName();
            if (ModelState.IsValid)
            {
                db.YAZAR.Add(yazar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(yazar);
        }

        // GET: /Yazar/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YAZAR yazar = db.YAZAR.Find(id);
            if (yazar == null)
            {
                return HttpNotFound();
            }
            return View(yazar);
        }

        // POST: /Yazar/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="yazarId,yazarAdi,yazarSoyadi,kayitTarihi,kullaniciId,guncellemeTarihi")] YAZAR yazar)
        {
            yazar.guncellemeTarihi = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(yazar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(yazar);
        }

        // GET: /Yazar/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YAZAR yazar = db.YAZAR.Find(id);
            if (yazar == null)
            {
                return HttpNotFound();
            }
            return View(yazar);
        }

        // POST: /Yazar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            YAZAR yazar = db.YAZAR.Find(id);
            db.YAZAR.Remove(yazar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
